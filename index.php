﻿<!DOCTYPE html>
<html>

<head>
    <title>Мастерская</title>  
    
    <style>
	
		@font-face {
			font-family: 'Montserrat-Regular';
			src: url('fonts/Montserrat-Regular.otf'); /* IE 9 Compatibility Mode */	
		}
		@font-face {
			font-family: 'Montserrat-Bold';
			src: url('fonts/Montserrat-Bold.otf'); /* IE 9 Compatibility Mode */	
		}
		
		@font-face {
			font-family: 'Montserrat-Light';
			src: url('fonts/Montserrat-Light.otf'); /* IE 9 Compatibility Mode */	
		}
		
		body, h1, h2, h3, h4, h5, h6, .h1, .h2, .h3, .h4, .h5, .h6, .popup-window, body div.bx-yandex-map, .fancybox-title {
			font-family: 'Montserrat-Regular';
		}
		
		.bold {font-family: 'Montserrat-Bold';}
		.light {font-family: 'Montserrat-Light';}

		html { /*overflow:hidden;*/ background: #fff; font-family: Open Sans,lucida grande,Segoe UI,arial,verdana,lucida sans unicode,tahoma,sans-serif; height: 100%; margin: 0; padding: 0; border: 0; font-size: 100%; font-style: normal; vertical-align: baseline; outline: 0;}
		body { margin:0; padding: 0; max-width: 100%; height: 100%; display: block; width: 100%;}
		* {box-sizing:border-box; margin:0; padding: 0;}
		a { text-decoration: none;}
		.container { margin: 0 auto; width: 70vw; position: relative; padding: 0 20px; background: #fff; height: 100%;  }
		
		.__main_text { padding-top: 20vh; position:relative;}
		.__main_text .__bottom_text {position:absolute; left: 79vh; bottom:0; top: 45.5vh;}
		.__main_text .__bottom_text p { cursor: default; text-transform:uppercase; font-size: 3.3vh; margin:0;}
		
		.__main_text .main_image { position: absolute; left: 0; top:0vh; right:0; height: 100vh; background-image: url('image/Mastersky-d.svg'); background-position:-30vh 0; background-size: auto 100vh; background-repeat: no-repeat;}
		
		.__main_text .__action_icon { transition: all 0.5s ease-in-out; animation: puls 1s ease-in-out infinite alternate-reverse; cursor: pointer; position:absolute; left: 105vh; top: 37vh; background-image: url('image/Asterisk.svg'); background-repeat: no-repeat; background-position: 50%; background-size: 7vh; width: 7vh; height: 7vh;}
		.__main_text .__action_icon p { opacity:0; position:absolute; left: 130%; top: 25%; font-size: 3vh; color: #ff1900; white-space:nowrap; margin:0; transition: all 0.5s ease-in-out;}
		.__main_text .__action_icon:hover p { opacity: 1;}
		
		.__main_nav { display:inline-block; width: 120vh; position:absolute; left:2vw; bottom: 25vh; right:0vw;}
		.__main_nav ul { display: flex; width: 100%; position:relative; padding:0;}
		.__main_nav ul:before { content: ""; position:absolute; left:0; right:0; height: 2px; background: #ff1900; top: 50%;}
		.__main_nav ul li {  transition: all 0.5s ease-in-out; background: #fff; text-transform:uppercase; font-weight: bold; font-size: 2vh; position: relative; list-style-type: none; flex-basis: auto; flex-grow: 1; padding-right: 0px; margin-right: 30px; list-style: none; text-align: center;}
    	.__main_nav ul li a {color: #ff1900;}
		.__main_nav ul li:last-child { margin:0; padding:0;}		
		
		.__main_nav ul li:hover a { color: #ce867f;}
		
		.__main_nav ul li p { opacity:0; cursor: default; position:absolute; left: 0%; top: 150%; right: 0; text-align:center; font-weight: 100; font-size: 1.5vh; color: #ff1900; white-space:nowrap; margin:0; transition: all 0.2s ease-in-out;}
		.__main_nav ul li:hover p { opacity: 1;}
    	
		.__home_page, .__next_page { position:absolute; left:0; right:0; top:0; bottom:0;}
		
		.__page { z-index: 0; opacity: 0; height: 100%; transition: all 0.2s ease-in-out;}
		.__page.is_active { opacity: 1; z-index: 9;}
		
		.__page_nav { position:absolute; right: 0%; top: 15%; width: 50px;}		
		.__page_nav.__left { left: 0%; right:auto;}
		
		.__page_nav > div { position:relative;}
		.__page_nav > div p { opacity:0; position:absolute; left: 110%; top: 25%; font-size: 2.2vh; color: #ff1900; white-space:nowrap; margin:0; transition: all 0.5s ease-in-out;}
		.__page_nav > div:hover p { opacity: 1;}
		
		.__page_nav.__left > div p { right: 110%; left:inherit;}
		
		.__close_page { display:inline-block; cursor: pointer; width: 6vh; height: 6vh; opacity: 1; transition: all 0.2s ease-in-out; background-image: url('image/menu/Close.svg'); background-position:center; background-repeat: no-repeat; background-size: 6vh;}
		.__close_page:hover { opacity: 1;}
		
		.__menu_btn { display:inline-block; cursor: pointer; width: 6vh; height: 6vh; opacity: 1; transition: all 0.2s ease-in-out; background-image: url('image/menu/Hamburger-idle-d.svg'); background-position:center; background-repeat: no-repeat; background-size: 6vh;}
   		.__menu_search { display:inline-block; cursor: pointer; width: 6vh; height: 6vh; opacity: 1; transition: all 0.2s ease-in-out; background-image: url('image/menu/Search-idle-d.svg'); background-position:center; background-repeat: no-repeat; background-size: 6vh;}
   		.__menu_i { display:inline-block; cursor: pointer; width: 6vh; height: 6vh; opacity: 1; transition: all 0.2s ease-in-out; background-image: url('image/menu/Left-handed-menu-d.svg'); background-position:center; background-repeat: no-repeat; background-size: 6vh;}
    	
		.__menu_btn:hover, .__menu_search:hover, .__menu_i:hover { opacity: 1;}
		
		.__page_next_wrap { padding: 20vh 10vh 0 10vh; height: 100vh; }
		.__page_next_title {color: #ff1900; font-size: 6vh; font-weight: 100; cursor: default;}
		
		.__block_text { display: flex; padding-top: 3vh;}
		.__block_text .item { padding: 10px 20px;}
		.__block_text .item:first-child { padding-left:0;}
		.__block_text .item:last-child { padding-right:0;}
		.__block_text .item .label { font-size: 2.4vh; color: #ff1900; padding-bottom: 30px;}
		.__block_text .item .text { text-align: left; color: #111; font-size: 2vh;}	
		
		@-webkit-keyframes puls { from {opacity: 0.5; } to {opacity: 1; } }
		
		@keyframes puls { from {opacity: 0.5; } to {opacity: 1; } }
		
    </style>
</head>



<body>
	<div class="__home_page __page is_active" data-page="1" style="height:100vh">
        <div class="container">    
            <div class="__main_text">            
                <p class="__t main_image"></p>                 
                <div class="__bottom_text">
                    <p>Медиатором <br>изобразительных<br> искусств - учебные<br> фонды ХГФ МПГУ <br> XX-XXL века</p>
                </div>    
                <div class="__action_icon __btn" data-page="2"><p>О проекте</p></div>
            </div>         
            <div class="__main_nav bold">
                <ul>
                    <li><a href="">Фонды</a><p class="light">О фондах</p></li>
                    <li><a href="">Журналы</a><p class="light">О журналах</p></li>
                    <li><a href="">Мастера</a><p class="light">О мастерских</p></li>
                    <li><a href="">Теория</a><p class="light">О терии</p></li>
                    <li><a href="">Практика</a><p class="light">О практике</p></li>
                    <li><a href="">Конкурс</a><p class="light">О кункурсах</p></li>
                </ul>
            </div>          
        </div>
    </div>
    
    <div class="__next_page __page " data-page="2">
    	<div class="container"> 
        	<div class="__page_nav __left">
            	<div class="__menu_i __btn_nav_ex"><p>Левшам</p></div>
            </div>
            
        	<div class="__page_nav __right">
        		<div class="__close_page __btn" data-page="1"><p>Назад</p></div>
                <div class="__menu_btn"><p>Меню</p></div>
                <div class="__menu_search"><p>Поиск</p></div>
            </div>  
            
            <div class="__page_next_wrap">
            	<p class="__page_next_title">Руководство</p>
                
                <div class="__block_text">
                	<div class="item">
                		<p class="label">О порядке эксплуатации</p>
                    	<p class="text">Для простого ознакомления с фондом достаточно воспользоваться селекцией категории. Для изыскания авторских работ – поиском по фамилии (полезно припомнить и добрачную). Не подписанные работы находяться по запросу #noname</p>
                	</div>
                    <div class="item">
                		<p class="label">О сохранности документов</p>
                    	<p class="text">Все существующие оригиналы, кроме работ с отметкой #notissued, будут доступны для возврата авторам или их наследникам в течении 1 года (12 месяцев – 365 дней) с момента окончания размещения архива (1 января 2021 года).</p>
                	</div>
                    <div class="item">
                		<p class="label">О способах их передачи</p>
                    	<p class="text">Забрать оригиналы можно по предварительной договорённости в районе метро Юго-Западная (не по какой-то символической причине, а из непростого совпадения), уведомив о желании письменно по адресу namaste@mastersky.icu</p>
                	</div>
                </div>
                
            </div>
                  
        </div>
    
    </div>
</body>

<script>
var active_page = 1;

document.querySelectorAll('.__btn').forEach(function(btn) {
	btn.addEventListener('click', function() {
		var page = this.dataset.page;
		console.log(page);
		document.querySelectorAll('.__page').forEach(function(el) { el.classList.remove('is_active');});
		
		document.querySelector('.__page[data-page="'+page+'"]').classList.add('is_active');
	});
});

document.querySelectorAll('.__btn_nav_ex').forEach(function(btn) {
	btn.addEventListener('click', function() {
		document.querySelectorAll('.__page_nav').forEach(function(el) { 
		
			if(el.classList.contains('__left')) {
				el.classList.add('__right');
				el.classList.remove('__left');
			} else if(el.classList.contains('__right')) {
				el.classList.remove('__right');
				el.classList.add('__left');
			}		
		});
		
	});
});

</script>

</html>